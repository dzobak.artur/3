﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace ConsoleApp11
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string fileName = "Student.json";

            
            CreateJsonFile(fileName);

            
            DisplayJsonFile(fileName);

           
            string searchLastName = "Dzobak";

            
            DisplayStudentInfoByLastName(fileName, searchLastName);

            
            string faculty = "FIT";
            int course = 3;
            CountExcellentMaleStudents(fileName, faculty, course);
        }

       
        class Student
        {
            public string LastName { get; set; }
            public string Faculty { get; set; }
            public int Course { get; set; }
            public string Gender { get; set; }
            public double Scholarship { get; set; }
            public List<double> Grades { get; set; }
        }

        
        static void CreateJsonFile(string fileName)
        {
            List<Student> students = new List<Student>
        {
            new Student
            {
                LastName = "Dzobak",
                Faculty = "ASF",
                Course = 1,
                Gender = "male",
                Scholarship = 1000.0,
                Grades = new List<double> { 90.5, 85.5, 88.0 }
            },
            new Student
            {
                LastName = "Pivkach",
                Faculty = "Fit",
                Course = 2,
                Gender = "male",
                Scholarship = 1200.0,
                Grades = new List<double> { 78.0, 88.5, 92.0 }
            },
            new Student
            {
                LastName = "Vogar",
                Faculty = "FIT",
                Course = 3,
                Gender = "male",
                Scholarship = 800.0,
                Grades = new List<double> { 95.0, 96.5, 91.0 }
            }
        };

            string json = JsonSerializer.Serialize(students, new JsonSerializerOptions
            {
                WriteIndented = true
            });


            File.WriteAllText(fileName, json);
            Console.WriteLine("JSON-файл створено: " + fileName);
        }

        
        static void DisplayJsonFile(string fileName)
        {
            string json = File.ReadAllText(fileName);
            Console.WriteLine("Вміст JSON-файлу:");
            Console.WriteLine(json);
        }

        
        static void DisplayStudentInfoByLastName(string fileName, string lastName)
        {
            string json = File.ReadAllText(fileName);
            List<Student> students = JsonSerializer.Deserialize<List<Student>>(json);

            Student student = students.Find(s => s.LastName == lastName);
            if (student != null)
            {
                Console.WriteLine("Прізвище: " + student.LastName);
                Console.WriteLine("Факультет: " + student.Faculty);
                Console.WriteLine("Курс: " + student.Course);
                Console.WriteLine("Стать: " + student.Gender);
                Console.WriteLine("Студентська стипендія: " + student.Scholarship);

                Console.WriteLine("Оцінки:");
                foreach (double grade in student.Grades)
                {
                    Console.WriteLine(grade);
                }
            }
            else
            {
                Console.WriteLine("Студента з прізвищем " + lastName + " не знайдено.");
            }
        }

       
        static void CountExcellentMaleStudents(string fileName, string faculty, int course)
        {
            string json = File.ReadAllText(fileName);
            List<Student> students = JsonSerializer.Deserialize<List<Student>>(json);

            int excellentCount = students.Count(s => s.Faculty == faculty && s.Course == course && s.Gender == "male" && s.Grades.All(grade => grade >= 90.0));

            Console.WriteLine($"Кількість хлопців-відмінників на {course}-му курсі факультету {faculty}: {excellentCount}");
        }
    }
}
